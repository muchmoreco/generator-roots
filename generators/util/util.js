module.exports = {
  slugify: (text, separator = '-')  => {
    return text.toString().toLowerCase()
      .replace(/\s+/g, separator)           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');            // Trim - from end of text
  },
  
  abbreviate: (str)  => {
    return str
      .split('-')
      .map(word => word.charAt(0) )
      .map(char => char.toUpperCase())
      join('');
  }

  
}