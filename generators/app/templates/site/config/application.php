
/*WP Migrate Pro Key*/
if (!defined('WPMDB_LICENCE')) {
  define('WPMDB_LICENCE', '<%= dbmigrate_licence =>');
}

/*Gravity Forms Key*/
if (!defined('GF_LICENSE_KEY')) {
  define('GF_LICENSE_KEY','<%= gf_licence %>');
}