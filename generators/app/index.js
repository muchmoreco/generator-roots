const perfy = require( 'perfy' );
perfy.start( 'yo' );

const Generator = require( 'yeoman-generator' );

const _ = require( 'lodash' );
const Joi = require( 'joi' );
const util = require( '../util/util' );
const yaml = require( 'js-yaml' );
const shell = require( 'shelljs' );
const chalk = require( 'chalk' );
const fs = require( 'fs' );
const yosay = require( 'yosay' );
const axios = require( 'axios' );
const ini = require( 'ini' );

const bitbucketApiUrl = 'https://api.bitbucket.org/2.0';

module.exports = class extends Generator {
  constructor ( args, opts ) {
    // Calling the super constructor is important so our generator is correctly set up
    super( args, opts );

    this.argument( 'app_name', { type: String, required: false } );
    this.appname = this.app_name || this.appname;
    this.appname = util.slugify( this.appname );

    // Next, add your custom code
    this.option( 'babel' ); // This method adds support for a `--babel` flag
    this.config.save(); //this saves .yo-rc.json file, which marks folder it resides in as project root

    this.bitbucketProjectKey = 'ROOTSIOWEBSITES';
    this.vaultsEncrypted = false;
    this.user = shell.exec( 'whoami' ).stdout.trim() || '...';
  }

  _validateDomain ( domain ) {
    const schema = Joi.string().regex( /^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/, 'domain' );
    const result = Joi.validate( domain, schema );

    return result.error === null || result.error.message;
  }

  async _validateBitbucketCreds ( creds ) {
    const schema = Joi.string().regex( /^[^\s]+:[^\s]+$/, 'credentials' ).required();
    const result = Joi.validate( creds, schema );

    if ( result.error !== null ) {
      return result.error.message;
    }

    const [ bitbucketUn, bitbucketPass ] = creds.split( ':' );
    const url = `${bitbucketApiUrl}/user`;
    try {
      const resp = await axios( {
        method: 'get',
        url: url,
        headers: { 'content-type': 'application/json' },
        auth: {
          username: bitbucketUn,
          password: bitbucketPass
        },
      } );

    } catch ( e ) {
      return `Could not authenticate with Bitbucket, please check credentials`;
    }

    return true;
  }

  _validatePassword ( password ) {
    const schema = Joi.string().required();
    const result = Joi.validate( password, schema );

    return result.error === null || result.error.message;
  }

  _notEmpty ( val ) {
    const schema = Joi.string().required();
    const result = Joi.validate( val, schema );

    return result.error === null || result.error.message;
  }

  async _uniqueBitbucketProjectKey ( key ) {
    const [ bitbucketUn, bitbucketPass ] = this.bitbucketCreds.split( ':' );
    const bitbucketTeam = this.bitbucketTeam;
    const url = `${bitbucketApiUrl}/teams/${bitbucketTeam}/projects/${key}`;

    try {
      const resp = await axios( {
        method: 'get',
        url: url,
        headers: { 'content-type': 'application/json' },
        auth: {
          username: bitbucketUn,
          password: bitbucketPass
        },
      } );

      //not unique
      return `Project with key "${key}" already exists. Please provide a unique key for it. Must be all UPPERCASE letters.`;
    } catch ( e ) {
    }

    return true;
  }

  async _uniqueBitbucketRepoName ( name ) {
    const [ bitbucketUn, bitbucketPass ] = this.bitbucketCreds.split( ':' );
    const bitbucketTeam = this.bitbucketTeam;
    const url = `${bitbucketApiUrl}/repositories/${bitbucketTeam}/${name}`;
    //this.log.error(url);

    try {
      const resp = await axios( {
        method: 'get',
        url: url,
        headers: { 'content-type': 'application/json' },
        auth: {
          username: bitbucketUn,
          password: bitbucketPass
        },
      } );

      //not unique
      return `Repo with name "${name}" already exists. Please provide a unique repo name.`;
    } catch ( e ) {
    }

    return true;
  }

  prompting () {

    this.log( yosay( chalk.green( `Hello ${this.user}! I will help you to scaffold a typical MuchMore Roots.io stack project` ) ) );

    const that = this;
    return this.prompt( [
      {
        type: 'list',
        name: 'tasks',
        message: 'How would you like to initialise this project?',
        choices: [
          { name: 'Start new project' },
          { name: 'Clone existing project', disabled: 'currently under construction' }
        ],
        validate: this._notEmpty,
        filter: function ( input ) {
          return input.toString().toLowerCase();
        },
        default: 0,
        store: true
      },
      {
        type: 'checkbox',
        name: 'environments',
        message: 'What project environments would you like to setup?',
        choices: [
          { name: 'Development', checked: true, disabled: false },
          { name: 'Staging', disabled: 'currently under construction' },
          { name: 'Production', disabled: 'currently under construction' }
        ],
        filter: function ( input ) {
          return input.toString().toLowerCase();
        },
        when: function ( answers ) {
          return answers.tasks === 'start new project';
        },
        store: true,
        validate: this._notEmpty
      },
      {
        type: 'input',
        name: 'projectName',
        message: 'Project name',
        default: this.appname, // Default to current folder name
        filter: function ( input ) {
          this.projectName = util.slugify( input );
          return this.projectName;
        },
        validate: this._notEmpty,
        store: true
      },
      {
        type: 'input',
        name: 'devDomain',
        message: 'Development Domain',
        default: `${util.slugify( this.appname )}.test`, // Default to current folder name
        filter: function ( input ) {
          return input.replace( /\.[a-z]*$/, '' ) + '.test'
        },
        validate: this._validateDomain,
        when: function ( answers ) {
          return answers.environments.split( ',' ).includes( 'development' )
        }
      },
      {
        type: 'confirm',
        name: 'enableDevSsl',
        message: 'Enable SSL on dev?',
        when: function ( answers ) {
          return answers.environments.split( ',' ).includes( 'development' )
        },
        default: false,
        store: true
      },
      {
        type: 'input',
        name: 'liveDomain',
        message: 'Production Domain',
        validate: this._validateDomain,
        when: answers => answers.tasks === 'start new project' && answers.environments.split( ',' ).includes( 'production' ),
        store: true
      },
      {
        type: 'password',
        name: 'password',
        message: 'Project-wide password',
        validate: this._validatePassword,
        when: function ( answers ) {
          return answers.tasks.split( ',' ).includes( 'start new project' )
        },
        default: '',
        store: true
      },
      {
        type: 'input',
        name: 'dbmigrate_licence',
        message: 'DB Migrate Pro license key (optional)',
        default: '',
        store: true
      },
      {
        type: 'input',
        name: 'gf_licence',
        message: 'Gravity Forms license key (optional)',
        default: '',
        store: true
      },
      {
        type: 'confirm',
        name: 'setupBitbucket',
        message: 'Would you like us to setup a Bitbucket repo for this project?',
        when: answers => answers.tasks === 'start new project',
        default: true,
        store: true
      },
      {
        type: 'input',
        name: 'bitbucketCreds',
        message: 'Provide your Bitbucket credentials in the following format username:password',
        validate: this._validateBitbucketCreds,
        when: answers => answers.setupBitbucket,
        default: '',
        store: true,
        filter: ( input ) => {
          this.bitbucketCreds = input;
          this.config.set( 'bitbucketCreds', input );
          return input;
        }
      },
      {
        type: 'input',
        name: 'bitbucketTeam',
        message: 'What is your Bitbucket team name?',
        validate: this._notEmpty, //todo: verify that team exists in Bitbucket account
        when: answers => answers.setupBitbucket,
        store: true,
        default: 'muchmoreco',
        filter: ( input ) => {
          this.bitbucketTeam = input;
          return input;
        }
      },
      /* {
         type    : 'input',
         name    : 'bitbucketProjectKey',
         message : 'Bitbucket Project Key? This is a short unique string of characters identifying a project e.g. PRCJ2 ',
         validate: this._uniqueBitbucketProjectKey.bind(that), //todo: verify that team exists in Bitbucket account
         when : answers => answers.setupBitbucket,
         store: true,
         default: `${util.slugify(this.appname, '')}`.toUpperCase(),
         filter: function(input) {
           return input.toUpperCase();
         },
       }, */
      {
        type: 'input',
        name: 'bitbucketRepoName',
        message: 'Bitbucket repository name?',
        validate: this._uniqueBitbucketRepoName.bind( that ),
        default: util.slugify( this.appname ),
        filter: function ( input ) {
          return util.slugify( input ).toLowerCase();
        },
        when: answers => answers.setupBitbucket,
      },

    ] ).then( ( answers ) => {
      this.answers = answers;
    } );
  }

  /**
   * Where you write the generator specific files (routes, controllers, etc)
   * @return void
   */
  async writing () {
    if ( this._getAnswer( 'tasks' ) === 'start new project' ) {
      await this._checkSoftwareDeps();
      this.log( `Setting up project: ${this.answers.projectName}` );
      //this._setupLocalGit();

      this._gotoProjectRoot();
      shell.mkdir( 'www' );

      //this._setupBitbucket();
      //this._setupTrellis();
      this._setupBedrock();
      //this._setupSage();
    } //start new project
  }

   async install () {
     await this._provisionDev();
     await this._setupLocalGit();
     await this._provisionStg();
     await this._provisionProd();
   }

  _encryptVaults () {
    this._gotoProjectRoot();
    this.log( 'Encrypting vaults' );
    const cmd = shell.exec( "cd www/trellis && vagrant ssh -- -t 'cd trellis; ansible-vault encrypt ./group_vars/all/vault.yml group_vars/development/vault.yml group_vars/staging/vault.yml group_vars/production/vault.yml'", { silent: true } );

    if ( cmd.code !== 0 ) {
      if ( cmd.stderr.includes( 'already encrypted' ) ) {
        this.log( chalk.green( 'Vaults are already encrypted, all is well.' ) );
        return true;
      }

      this.log.error( 'Could not encrypt vaults' );
      return false;
    }

    return true;
  }

  async _setupLocalGit () {

    let codes = [];
    if ( this._encryptVaults() ) {
      const file = 'www/.gitignore';

      try {
        this.fs.copyTpl(
          this.templatePath( '_gitignore' ),
          this.destinationPath( file )
        );

        this.fs.commit( [], () => {
        } );

        this.log('Copy Custom project .gitignore');

      } catch ( e ) {
        this.log.error( `Could not customise ${file}` );
        this.log.error( e );
      }

      this._gotoWWW();

      codes.push( shell.exec( 'git init' ).code );
      codes.push( shell.exec( 'git add .' ).code );

      //this.log( shell.exec( 'git status' ).stdout );
      codes.push( shell.exec( 'git commit -m "Initial import"', {silent: true} ).code );

      if ( this.answers.setupBitbucket ) {
        this.log( chalk.green() )
        codes.push( shell.exec( `git remote add --track master origin git@bitbucket.org:${this.answers.bitbucketTeam}/${this.answers.bitbucketRepoName}.git || git remote set-url origin git@bitbucket.org:${this.answers.bitbucketTeam}/${this.answers.bitbucketRepoName}.git` ).code );
        codes.push( shell.exec( 'git push origin --all' ) );
      }

      //check if there were any errors while creating a git repo
      if ( codes.reduce( ( a, b ) => a + b, 0 ) > 0 ) {
        this.log.error( 'There were issues while creating a local GIT repo. Please do manual checks' );
      }

    } else {
      this.log.error( 'Vaults are not encrypted. It a security hazard to commit the proejct to the repo at this point. Skipping ' )
    }

  }

  _provisionDev () {
    if ( this._getAnswer( 'environments' ).split( ',' ).includes( 'development' ) ) {
      this._gotoWWW();
      shell.cd( 'trellis' );

      //create trellis vm
      if ( shell.exec( 'vagrant up' ).code !== 0 ) {
        this.log.error( 'Could not setup Development VM' );
        shell.exit( 1 );
      }

      //activate sage theme
      const cmd = shell.exec( `vagrant ssh -- -t 'wp theme activate sage/resources --path=/srv/www/${this.answers.projectName}/current/web/wp'` );

      if ( cmd.code !== 0 ) {
        this.log.error( 'Could not activate sage theme' );
      }

    }
  }

  _provisionStg () {
    if ( this._getAnswer( 'environments' ).split( ',' ).includes( 'staging' ) ) {
      //todo: complete method
    }
  }

  _provisionProd () {
    if ( this._getAnswer( 'environments' ).split( ',' ).includes( 'production' ) ) {
      //todo: complete method
    }
  }

  /**
   * checks if all  software  prerequisits are satisfied
   * @return {[type]} [description]
   */
  _checkSoftwareDeps () {
    if ( !shell.which( 'git' ) ) {
      this.log.error( 'Sorry, this script requires git. Plesae install.' );
      shell.exit( 1 );
    } else {
      this.log( chalk.green( 'Git installed. Check!' ) );
    }

    if ( !shell.which( 'composer' ) ) {
      this.log.error( 'Sorry, this script requires composer. Please install.' );
      shell.exit( 1 );
    } else {
      this.log( chalk.green( 'Composer installed. Check!' ) );
    }
  }

  /**
   * get answer by key or false if no key found
   * @param  {String} key answer key
   * @return {String|Bool}     answer string or false if no key found
   */
  _getAnswer ( key ) {
    return (Object.keys( this.answers ).includes( key )) ? this.answers[ key ] : false;
  }

  /**
   * jump to project root directory
   */
  _gotoProjectRoot () {
    shell.cd( this.destinationRoot() );
  }

  /**
   * www folder contains roots stack files, cd into it
   */
  _gotoWWW () {
    this._gotoProjectRoot();
    shell.cd( 'www' );
  }

  async _setupBitbucket () {
    this.log( chalk.red( "_setupBitbucket" ) );
    if ( this.answers.setupBitbucket ) {

      const [ bitbucketUn, bitbucketPass ] = this.answers.bitbucketCreds.split( ':' );
      this.log( "Setting up a repo in Bitbucket" );

      try {
        var resp = await axios( {
          method: 'post',
          url: `${bitbucketApiUrl}/repositories/${this.answers.bitbucketTeam}/${this.answers.bitbucketRepoName}/`,
          headers: { 'content-type': 'application/json' },
          auth: {
            username: bitbucketUn,
            password: bitbucketPass
          },
          data: {
            scm: 'git',
            name: this.answers.projectName,
            project: { key: this.bitbucketProjectKey },
            description: `MuchMore roots stack website ${this.answers.bitbucketRepoName}`,
            is_private: true
          }
        } );

        this.log( chalk.green( "Bitbucket repo created successfully." ) );
      } catch ( e ) {
        //could not create a repo
        this.log.error( 'Could not create a repo' );
        this.log.error( e );
      }

    }
  }

  /**
   * clone and setup Trellis in www folder
   */
  _setupTrellis () {
    this.log( chalk.red( "_setupTrellis" ) );
    this._gotoWWW();

    // Clone Trellis
    if ( shell.ls( 'trellis' ).code !== 0 || shell.exec( 'find trellis -type d -empty' ).stdout > '' ) {
      if ( shell.exec( 'git clone --depth=1 https://github.com/roots/trellis.git trellis && rm -rf trellis/.git' ).code !== 0 ) {
        this.log.error( 'Could not clone Trellis' );
        shell.exit( 1 );
      }
    } else {
      this.log( 'Trellis is already installed, skipping.' );
    }

    // setup deploy hooks
    try {
      this.fs.copyTpl(
        this.templatePath( 'trellis/deploy-hooks/build-before.yml' ),
        this.destinationPath( 'www/trellis/deploy-hooks/build-before.yml' )
      )
      //fs.writeFileSync(this.destinationPath(usersFile), yaml.dump(allUsers));
    } catch ( e ) {
      this.log.error( `Could not customise build-before.yml` );
      this.log.error( e );
    }

    // setup nginx includes
    try {
      this.fs.copyTpl(
        this.templatePath( 'trellis/nginx-includes/nginx.conf.child' ),
        this.destinationPath( 'www/trellis/nginx-includes/nginx.conf.child' )
      )
      //fs.writeFileSync(this.destinationPath(usersFile), yaml.dump(allUsers));
    } catch ( e ) {
      this.log.error( `Could not customise nginx.conf.child` );
      this.log.error( e );
    }


    // customise trellis yml files
    this._setupAllGroupVars();
    this._setupDevGroupVars();
    this._setupStgGroupVars();
    this._setupProdGroupVars();
    this._setupVaultPass();

  }

  _setupVaultPass () {
    const file = 'www/trellis/.vault_pass';
    try {
      this.fs.copyTpl(
        this.templatePath( 'trellis/.vault_pass' ),
        this.destinationPath( file ),
        this.answers
      );
      //fs.writeFileSync(this.destinationPath(usersFile), yaml.dump(allUsers)); 
    } catch ( e ) {
      this.log.error( `Could not customise ${file}` );
      this.log.error( e );
    }

    this.log( 'Updating ansible.cfg' );
    const ansibleConf = ini.parse( this.fs.read( this.destinationPath( 'www/trellis/ansible.cfg' ) ) );
    ansibleConf.defaults.vault_password_file = '.vault_pass';
    this.fs.write( this.destinationPath( 'www/trellis/ansible.cfg' ), ini.stringify( ansibleConf ) );

  }

  _setupAllGroupVars () {

    const usersFile = 'www/trellis/group_vars/all/users.yml';
    const securityFile = 'www/trellis/group_vars/all/security.yml';
    const mainFile = 'www/trellis/group_vars/all/main.yml';

    //copy group_vars/all/users.yml from templates to destination
    try {
      this.fs.copyTpl(
        this.templatePath( 'trellis/group_vars/all/users.yml' ),
        this.destinationPath( usersFile )
      );
    } catch ( e ) {
      this.log.error( `Could not customise ${usersFile}` );
      this.log.error( e );
    }

    //copy group_vars/all/main.yml from templates to destination
    try {
      this.fs.copyTpl(
        this.templatePath( 'trellis/group_vars/all/main.yml' ),
        this.destinationPath( mainFile )
      );
    } catch ( e ) {
      this.log.error( `Could not customise ${mainFile}` );
      this.log.error( e );
    }

    // update necessary vars directly in the destination file
    try {
      let allSecurity = yaml.load( fs.readFileSync( this.destinationPath( securityFile ), 'utf8' ) );
      allSecurity[ 'sshd_permit_root_login' ] = false;
      this.fs.write( this.destinationPath( securityFile ), yaml.dump( allSecurity ) );
    } catch ( e ) {
      this.log.error( `Could not customise ${securityFile}` );
      this.log.error( e );
    }
  }

  _setupDevGroupVars () {
    if ( this.answers.environments.split( ',' ).includes( 'development' ) ) {
      this.log( 'Setting up Trells group vars for Development environment.' );

      const vaultFile = 'www/trellis/group_vars/development/vault.yml';
      const wordpressSitesFile = 'www/trellis/group_vars/development/wordpress_sites.yml';

      // shell.sed('-i', 'example.com', `${this.answers.projectName}`,this.destinationPath('www/trellis/group_vars/development/*.yml'));
      // shell.sed('-i', 'example.test', `${this.answers.devDomain}`,this.destinationPath(wordpressSitesFile));

      try {
        this.fs.copyTpl(
          this.templatePath( 'trellis/group_vars/development/vault.yml' ),
          this.destinationPath( vaultFile ),
          this.answers
        );
        // const devVault = yaml.load(this.fs.read(this.destinationPath(vaultFile)));

        // vaultYml.repla
        //let allUsers = yaml.load(fs.readFileSync(this.destinationPath(usersFile), 'utf8'));
        //allUsers = _.assign(allUsers, allUsersExtras);

        //this.write(this.destinationPath(vaultFile), yaml.dump(devVault));
        //this.fs.copyTpl(
        //  this.templatePath('trellis/group_vars/all/users.yml'),
        //  this.destinationPath('www/trellis/group_vars/all/users.yml')
        //);

        //fs.writeFileSync(this.destinationPath(usersFile), yaml.dump(allUsers)); 
      } catch ( e ) {
        this.log.error( `Could not customise ${vaultFile}` );
        this.log.error( e );
      }

      try {
        this.fs.copyTpl(
          this.templatePath( 'trellis/group_vars/development/wordpress_sites.yml' ),
          this.destinationPath( wordpressSitesFile ),
          this.answers
        );

      } catch ( e ) {
        this.log.error( `Could not customise ${wordpressSitesFile}` );
        this.log.error( e );
      }


    }
  }

  _setupStgGroupVars () {
    if ( this.answers.environments.split( ',' ).includes( 'staging' ) ) {

    }
  }

  _setupProdGroupVars () {
    if ( this.answers.environments.split( ',' ).includes( 'production' ) ) {

    }
  }

  /**
   * clone and setup Bedrock in www folder
   */
  _setupBedrock () {
    this.log( chalk.red( '_setupBedrock' ) );
    this._gotoWWW();

    // Clone Bedrock
    if ( shell.ls( 'site' ).code !== 0 ) {
      this.log( chalk.green( 'Cloning Bedrock' ) );

      if ( shell.exec( 'git clone --depth=1 https://github.com/roots/bedrock.git site && rm -rf site/.git' ).code !== 0 ) {
        this.log.error( 'Could not clone Bedrock' );
        shell.exit( 1 );
      }
    } else {
      this.log( 'Bedrock is already installed, skipping.' );
    }

    if ( !this.fs.read( this.destinationPath( 'www/site/config/application.php' ) ).includes( 'WPMDB_LICENCE' ) && this.answers.dbmigrate_licence > '' ) {
      this.log( chalk.green( 'Add custom configs1' ) );
      let application_config = this.fs.read( this.templatePath( 'site/config/application.php' ) );
      application_config = application_config.replace( '<%= dbmigrate_licence =>', this.answers.dbmigrate_licence );
      this.fs.append( this.destinationPath( 'www/site/config/application.php' ), application_config );
    }

    if ( !this.fs.read( this.destinationPath( 'www/site/config/application.php' ) ).includes( 'GF_LICENSE_KEY' ) && this.answers.gf_licence > '' ) {
      this.log( chalk.green( 'Add custom configs2' ) );
      let application_config = this.fs.read( this.templatePath( 'site/config/application.php' ) );
      application_config = application_config.replace( '<%= gf_licence =>', this.answers.gf_licencee );
      this.fs.append( this.destinationPath( 'www/site/config/application.php' ), application_config );
    }

    this.log( chalk.green( 'Adding Bedrock composer deps' ) );
    //read and parse template Bedrock composer.json
    const pkgJson = this.fs.readJSON( this.templatePath( 'bedrock/composer.json' ) );
    //extend Bedrock's composer.json with the config from template
    this.fs.extendJSON( this.destinationPath( 'www/site/composer.json' ), pkgJson );

    this.log( chalk.green( 'Adding custom .gitattributes to site' ) );
    this.fs.copyTpl(
      this.templatePath( 'site/_gitattributes' ),
      this.destinationPath( 'www/site/.gitattributes' )
    );

    this.fs.commit( [], () => {
      this._gotoWWW();

      //install bedrock composer deps
      this.log( chalk.green( 'Running Bedrock composer install' ) );
      shell.cd( 'site' )
      shell.exec( 'composer update' );
    } );

  }


  /**
   * setup Sage 9 theme in bedrock themes folder
   */
  _setupSage () {
    this._gotoWWW();

    if ( shell.ls( 'site/web/app/themes/sage' ).code !== 0 || shell.exec( 'find site/web/app/themes/sage -type d -empty' ).stdot > '' ) {
      if ( shell.exec( 'cd site/web/app/themes && composer create-project roots/sage sage' ).code !== 0 ) {
        this.log.error( 'Could not setup Sage 9' );
        shell.exit( 1 );
      }
    } else {
      this.log( 'Sage is already installed, skipping.' );
    }

    this._gotoWWW();

    //install yarn deps
    if ( shell.ls( 'site/web/app/themes/sage/node_modules' ).code !== 0 ) {
      shell.cd( 'site/web/app/themes/sage' )
      //todo: remove comment
      //shell.exec( 'yarn' );
    } else {
      this.log( 'Sage 9 yarn deps have already been installed, skipping' );
    }

    //setup proxy
    this._gotoProjectRoot();

    const protocol = `http${this.answers.enableDevSsl ? 's' : ''}`;
    const sageConfigFile = 'www/site/web/app/themes/sage/resources/assets/config.json';
    const devUrl = `${protocol}://${this.answers.devDomain}`;
    const proxyUrl = `${protocol}://localhost:3000`;

    try {
      this.fs.copyTpl(
        this.templatePath( 'sage/app/resources/assets/config.json' ),
        this.destinationPath( sageConfigFile ),
        { devUrl, proxyUrl }
      );

    } catch ( e ) {
      this.log.error( `Could not customise ${sageConfigFile}` );
      this.log.error( e );
    }

    try {
      this.fs.copyTpl(
        this.templatePath( 'sage/.stylelintrc' ),
        this.destinationPath( 'www/site/web/app/themes/sage/.stylelintrc' )
      );

    } catch ( e ) {
      this.log.error( `Could not customise .stylelintrc` );
      this.log.error( e );
    }

    try {
      this.fs.copyTpl(
        this.templatePath( 'sage/.editorconfig' ),
        this.destinationPath( 'www/site/web/app/themes/sage/.editorconfig' )
      );

    } catch ( e ) {
      this.log.error( `Could not customise .editorconfig` );
      this.log.error( e );
    }
  }

  end () {
    this.log( yosay( `${this.user}, we are done here! ...and it only took us ${perfy.end( 'yo' ).time}s` ) );
  }
};